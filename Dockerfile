FROM node:8.11.3-alpine

RUN yarn global add typescript
RUN mkdir -p  /usr/src/app
WORKDIR /usr/src/app
ADD . ./
RUN cp src/configs/config.template.ts src/configs/config.ts
RUN yarn install
RUN tsc

CMD node dist/index.js