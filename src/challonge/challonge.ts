import challonge from 'challonge';
import * as Discord from 'discord.js';
import { config } from '../configs/config';

export class ChallongeApi {
    
    private apiKey: string;
    private client: any;
    
    constructor(apiKey: string|undefined) {
        this.apiKey = apiKey || "";
        this.client = challonge.createClient({
            apiKey: this.apiKey
        });
        
    }
    
    public isAdmin(member: Discord.GuildMember): boolean {
        const adminGroups = process.env.ADMIN_GROUPS;
        if (adminGroups) {
            return adminGroups.split(',').find(v => v === member.highestRole.name) !== undefined;
        }
        return config.adminGroups.find(v => v === member.highestRole.name) !== undefined;
    }
    
    public async parseDiscordMessageAndRespond(message: Discord.Message): Promise<string> {
        
        const isAdmin = this.isAdmin(message.member);
        const arg = message.content.slice(2, message.content.length).split(' ');
        
        if (arg.length > 0) {
            switch (arg[0]) {
                case 'create': 
                return isAdmin ? this.createTournament(arg.slice(1, arg.length)) : this.unauthorizeResponse();
                case 'show':
                return this.showTournaments();
                case 'delete':
                return isAdmin ? this.deleteTournament(arg.slice(1, arg.length)) : this.unauthorizeResponse();
                case 'update':
                return isAdmin ?  this.addTournamentDescription(arg.slice(1, arg.length)) : this.unauthorizeResponse();
                case 'add':
                return isAdmin ?  this.addMemberTournament(arg.slice(1, arg.length)) : this.unauthorizeResponse();
                case 'join':
                return this.joinTournament(arg.slice(1, arg.length), message.member);
                case 'hajime':
                return isAdmin ?  this.startTournament(arg.slice(1, arg.length)) : this.unauthorizeResponse();
                case 'matches':
                return isAdmin ?  this.showMatches(arg.slice(1, arg.length)) : this.unauthorizeResponse();
                case 'part':
                return this.showParticipants(arg.slice(1, arg.length)) ;
                default:
                return this.helpCommand();
            }
            
        } else {
            return this.helpCommand();
        }
    }
    
    public createTournament(parameters: string[]): Promise<string>  {
        if (parameters.length !== 4) {
            return this.helpCommand();
        }
        return new Promise<string>(reslove => {
            this.client.tournaments.create({
                tournament: {
                    url: parameters[0],
                    name: parameters[1],
                    start_at: parameters[2],
                    signup_cap: parameters[3] !== "0" ? parameters[3] : null
                },
                callback: (err: any, data: any) => {
                    if (err) {
                        console.error('ERROR', err);
                        reslove('Sorry I cannot create the tournament, reason : ' + JSON.stringify(err.text.errors));
                    } else {
                        console.log('Data', data.tournament.url);
                        reslove('https://challonge.com/'+ data.tournament.url);
                    }
                }
            });
        });
    }
    
    public addTournamentDescription(parameters: string[]): Promise<string>  {
        if (parameters.length < 2) {
            return this.helpCommand();
        }
        const id = parameters[0];
        const description = parameters.slice(1, parameters.length).join(' ');
        
        return new Promise<string>(reslove => {
            
            this.client.tournaments.update({
                id: id,
                tournament: {
                    description: description,
                },
                callback: (err: any, data: any) => {
                    if (err) {
                        console.error('ERROR', err);
                        // reslove('Sorry I cannot update the tournament, reason : ' + JSON.stringify(err.text.errors));
                    } else {
                        console.log('Data', data.tournament.description);
                        // reslove('New description : '+ data.tournament.description);
                    }
                }
            });
            
        });
    }
    
    public deleteTournament(parameters: string[]): Promise<string>  {
        if (parameters.length !== 1) {
            return this.helpCommand();
        }
        return new Promise<string>(reslove => {
            this.client.tournaments.destroy({
                id: parameters[0],
                callback: (err: any, data: any) => {
                    if (err) {
                        console.error('ERROR', err);
                        reslove('Sorry I cannot delete the tournament, reason : ' + JSON.stringify(err.text.errors));
                    } else {
                        console.log('Delete', data.tournament.url);
                        reslove(data.tournament.url + ' deleted');
                    }
                }
            });
        });
    }
    
    public joinTournament(parameters: string[], member: Discord.GuildMember): Promise<string>  {
        if (parameters.length !== 1) {
            return this.helpCommand();
        }
        return new Promise<string>(reslove => {
            this.client.participants.create({
                id: parameters[0],
                participant: {
                    name: member.user.username
                },
                callback: (err: any, data: any) => {
                    if (err) {
                        console.error(err);
                        reslove('Sorry I cannot add you, reason : ' + JSON.stringify(err.text.errors) + ' ' + member.user.username);
                    }         
                    reslove('You\'re added to the tournament ' + parameters[0] + ' as ' + data.participant['name']);
                }
            });
        });
    }
    
    public addMemberTournament(parameters: string[]): Promise<string>  {
        console.log(parameters)
        if (parameters.length < 2) {
            return this.helpCommand();
        }
        const id = parameters[0];
        const promiseArray: any = [];
        for(let p of parameters.slice(1, parameters.length)) {
            promiseArray.push(new Promise<string>(_resolve => {this.client.participants.create({
                id: id,
                participant: {
                    name: p
                },
                callback: (err: any, data: any) => {
                    if (err) {
                        console.error(err);
                        _resolve('Unable to add ' + p + ' reason : ' + JSON.stringify(err.text.errors) + '\n' );
                    }         
                    _resolve(p + ' added to the tournament\n');
                }})}))
            }
            console.log
            return new Promise<string>(reslove => {
                Promise.all(promiseArray).then(res => {
                    reslove('Infos :\n'+ res );
                });
                
            });
        }
        
        public showTournaments(): Promise<string>  {
            console.log('show tournaments')
            return new Promise<string>(reslove => {
                this.client.tournaments.index({
                    callback: (err: any, data: any) => {
                        if (err) {
                            console.error(err);
                        }         
                        let response = '```\n'
                        const dataArray = Object.entries(data);
                        dataArray.forEach((t: any) => {
                            console.log(t);
                            response += 'Tournoi : ' + t[0] + '\n';
                            response += 'id : ' + t[1].tournament.url + '\n';
                            response += 'name : ' + t[1].tournament.name + '\n';
                            response += 'url : https://challonge.com/' + t[1].tournament.url + '\n';
                            response += 'infos : ' + t[1].tournament.description + '\n';
                            response += '\n';
                        });
                        response += '```';
                        reslove(response)
                    }
                });
            });
        }
        
        public startTournament(parameters: string[]): Promise<string>  {
            if (parameters.length !== 1) {
                return this.helpCommand();
            }
            return new Promise<string>(reslove => {
                this.client.tournaments.start({
                    id: parameters[0],
                    callback: (err: any, data: any) => {
                        if (err) {
                            console.error(err);
                            reslove('Sorry I cannot start the tournament, reason : ' + JSON.stringify(err.text.errors));
                        }         
                        const date = new Date(data.tournament['startdAt'])
                        let response = '```\n'
                        response += 'The tournament begin ! HAAAAAAAAAAAAJIIIIIIMMEEEEEEEE';
                        response += '```';
                        reslove(response)
                    }
                });
            });
        }
        
        public showParticipants(parameters: string[]): Promise<string>  {
            if (parameters.length !== 1) {
                return this.helpCommand();
            }
            return new Promise<string>(reslove => {
                this.client.participants.index({
                    id: parameters[0],
                    callback: (err: any, data: any) => {
                        if (err) {
                            console.error(err);
                            reslove('Sorry I cannot show you the participation of the tournament, reason : ' + JSON.stringify(err.text.errors));
                        }         
                        let response = '```\n'
                        response += 'Tournoi : ' + parameters[0] + '\n';
                        const dataArray = Object.entries(data);
                        dataArray.forEach((t: any) => {
                            console.log(t);
                            response += '- ' + t[1].participant.name + '\n';
                        });
                        response += '```';
                        reslove(response)
                    }
                });
            });
        }
        
        public async showMatches(parameters: string[]): Promise<string>  {
            if (parameters.length !== 1) {
                return this.helpCommand();
            }
            return new Promise<string>(reslove => {
                this.client.matches.index({
                    id: parameters[0],
                    callback:  async (err: any, data: any) => {
                        if (err) {
                            console.error(err);
                            reslove('Sorry I cannot show you the list of matches, reason : ' + JSON.stringify(err.text.errors));
                        }         
                        let response = '```\n'
                        console.log(data)
                        // response += 'Tournoi : ' + parameters[0] + '\n';
                        const dataArray = Object.entries(data);
                        const open: any = [];
                        const pending: any = [];
                        dataArray.forEach((m: any) => {
                            
                            if (m[1].match['state'] === 'open') {
                                open.push(m[1].match)
                            } else if (m[1].match['state'] === 'pending') {
                                pending.push(m[1].match)
                            }
                        });
                        response += 'List of actives matches\n'
                        for(const p of open) {
                            const p1 = await this.getPlayerNameById(parameters[0], p.player1Id);
                            const p2 = await this.getPlayerNameById(parameters[0], p.player2Id);
                            response +=  p1 + ' VS ' + p2 + '\n'
                        }
                        response += '\nList of pending matches\n'
                        for(const p of pending) {
                            const p1 = await this.getPlayerNameById(parameters[0], p.player1Id);
                            const p2 = await this.getPlayerNameById(parameters[0], p.player2Id);
                            response +=  p1 + ' VS ' + p2 + '\n'
                        }
                        response += '```';
                        reslove(response)
                    }
                });
            });
        }
        
        private async getPlayerNameById(tournamentId: string, playerId: string): Promise<string> {
            return new Promise<string>(resolve => {
                this.client.participants.show({
                    id: tournamentId,
                    participantId: playerId,
                    callback: (err: any, data: any) => {
                        if (err) {
                            console.error(err);
                            resolve('Nobody =D');
                        } else {
                            console.log(data.participant.name)
                            resolve(data.participant.name);
                        }
                    }
                })
            });
        }
        
        public helpCommand(): Promise<string> {
            return new Promise<string>(reslove => {
                let helpMessage = "Discord Bot by TheGreatGodMikaXII\n\n"
                helpMessage += "```Markdown\n"
                helpMessage += "**c!create**\n"
                helpMessage += "Function: Creates a challonge tournament\n"
                helpMessage += "Usage: c!create <name> <game> <date> <participant-amount>\n"
                helpMessage += "Example:\n"
                helpMessage += "- c!create RecalTournaments bomberman 2018-07-19 16\n"
                helpMessage += "- c!create RecalTournaments bomberman 2018-07-19 0 <0 = unlimited>\n"
                helpMessage += "```\n"
                helpMessage += "```Markdown\n"
                helpMessage += "**c!show**\n"
                helpMessage += "Function: show list of current tournaments\n"
                helpMessage += "Usage: c!show\n"
                helpMessage += "```\n"
                helpMessage += "```Markdown\n"
                helpMessage += "**c!update**\n"
                helpMessage += "Function: Add description to a tournament\n"
                helpMessage += "Usage: c!update <url> <desctiption>\n"
                helpMessage += "Example: c!update recalTournament new description for this description\n"
                helpMessage += "```\n"
                helpMessage += "```Markdown\n"
                helpMessage += "**c!delete**\n"
                helpMessage += "Function: Delete a tournament\n"
                helpMessage += "Usage: c!delete <tournament-id>\n"
                helpMessage += "Example: c!delete recalTournament\n"
                helpMessage += "```\n"
                helpMessage += "```Markdown\n"
                helpMessage += "**c!join**\n"
                helpMessage += "Function: join a tournament\n"
                helpMessage += "Usage: c!join <tournament-id>\n"
                helpMessage += "```\n"
                helpMessage += "```Markdown\n"
                helpMessage += "**c!add**\n"
                helpMessage += "Function: add one or more participant a tournament\n"
                helpMessage += "Usage: c!add <tournament-id> <player1> ... <playerX>\n"
                helpMessage += "Example: c!add MikaXII Oyyo Acris\n"
                helpMessage += "```\n"
                helpMessage += "```Markdown\n"
                helpMessage += "**c!hajime**\n"
                helpMessage += "Function: start a tournament\n"
                helpMessage += "Usage: c!add <tournament-id> \n"
                helpMessage += "```\n"
                helpMessage += "```Markdown\n"
                helpMessage += "**c!part**\n"
                helpMessage += "Function: show participant of a tournament\n"
                helpMessage += "Usage: c!part <tournament-id> \n"
                helpMessage += "```\n"
                helpMessage += "```Markdown\n"
                helpMessage += "**c!matches**\n"
                helpMessage += "Function: show matches of a tournament\n"
                helpMessage += "Usage: c!matches <tournament-id> \n"
                helpMessage += "```\n"
                
                
                reslove(helpMessage);
            });
        }
        
        public unauthorizeResponse(): Promise<string> {
            return new Promise<string>(reslove => {
                let helpMessage = "NOPE YOU CAN'T RUN THIS COMMAND !!!!!\n";
                reslove(helpMessage);
            });
        }
    }