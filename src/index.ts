import * as Discord from 'discord.js';
import { ChallongeApi } from './challonge/challonge';
import { config } from './configs/config';


const client = new Discord.Client();

const challongeApiKey = process.env.CHALLONGE_API_KEY;
const discordToken = process.env.DISCORD_TOKEN;
const challongeApi = new ChallongeApi(challongeApiKey || config.challongeApiKey);

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`); 
});

client.on('message', async (msg: Discord.Message) => {
    if (msg.content.startsWith('c!')) {
        
        //const isAdmin = config.AdminGroups.find(v => v === msg.member.highestRole.name) !== undefined;
        const response = await challongeApi.parseDiscordMessageAndRespond(msg)
        msg.reply(response);

    }
});
client.login(discordToken || config.discordToken);
